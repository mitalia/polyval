#ifndef POLYVAL_H_INCLUDED
#define POLYVAL_H_INCLUDED

#define POLYVAL(T) \
    virtual void move_me(void *target) override {\
        new (target)T(std::move(*this));\
    }\
    \
    virtual void copy_me(void *target) const override {\
        new (target)T(*this);\
    }\
    \
    virtual void delete_me() override {\
        this->~T(); \
    }

struct polyval {
    virtual void copy_me(void *target) const = 0;
    virtual void move_me(void *target) = 0;
    virtual void delete_me() = 0;
    virtual ~polyval() {}
};

#define POLYVAL_CONT_BEG(T, base) \
    struct T {\
        base *operator->() { return get(); } \
        const base *operator->() const { return get(); } \
        base &operator*() { return *get(); } \
        const base &operator*() const { return *get(); } \
        base *get() { return (base *)&data; } \
        const base *get() const { return (base *)&data; } \
        template<typename U> T(const U& u) : data(u) {} \
        template<typename U> T(const U&& u) : data(u) {} \
        union Data { \
            Data(const Data& other) { ((const base&)other).copy_me(this); } \
            Data(Data&& other) { ((base&&)other).move_me(this); } \
            Data &operator=(Data&& other) { \
                ((base *)this)->delete_me(); \
                ((base &&)other).move_me(this); \
                return *this; \
            }\
            ~Data() { ((base *)this)->delete_me(); } \

#define POLYVAL_TOKENPASTE(x, y) x ## y
#define POLYVAL_TOKENPASTE2(x, y) POLYVAL_TOKENPASTE(x, y)
#define POLYVAL_CONT_ELEM(T) \
        T POLYVAL_TOKENPASTE2(t_, __LINE__); Data(const T& el) : POLYVAL_TOKENPASTE2(t_, __LINE__) (el) {} Data(T&& el) : POLYVAL_TOKENPASTE2(t_, __LINE__) (el) {}

#define POLYVAL_CONT_END \
        } data; \
    };

#endif // POLYVAL_H_INCLUDED
