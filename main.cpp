#include <iostream>
#include "polyval.h"

#define IMPL(T) \
    virtual void greet() override { puts("Hello from " #T); } \
    virtual ~T() { puts("~" #T); }

struct A : polyval {
    virtual void greet() = 0;
};

struct B : A {
    POLYVAL(B)
    IMPL(B)
};

struct C : A {
    POLYVAL(C)
    IMPL(C)
};

struct D : C {
    POLYVAL(D)
    IMPL(D)
};

POLYVAL_CONT_BEG(A_cont, A)
    POLYVAL_CONT_ELEM(B)
    POLYVAL_CONT_ELEM(C)
    POLYVAL_CONT_ELEM(D)
POLYVAL_CONT_END

int main()
{
    B b;
    C c;
    D d;
    A_cont c_b(b);
    A_cont c_b2(c_b);
    A_cont c_c(c);
    c_b->greet();
    c_b2->greet();
    c_c->greet();
    return 0;
}
