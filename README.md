# PolyVal #

### a value-semantics wrapper for polymorphic class hierarchies ###

In C++ if you are interested in polymorphic class behavior you are pretty much forced to use heap (AKA freestore AKA `new`) allocation, and carry around (smart) pointers.

This isn't generally much of a problem, as often instances of polymorphic classes are already conceptually "reference types" (in the .NET/Swift `class` meaning), but there are occasions where you'd like to carry around polymorphic classes that are actually *values* (again, pretty much in the .NET/Swift `struct` meaning); that means mostly automatic copying *but without slicing*.

Enters `PolyVal`; with a horrible kludge of `union` and macros, you can define a value that can hold an instance of classes derived from some base class, without external allocation and with automatic, non-slicing copy. The `->` operator is overloaded to transparently operate on the base class interface.

Of course this requires modifying the various derived classes (to add the relevant virtual methods for placement `new` and destruction), and for the derived classes to host to be known when the container type is defined (otherwise it wouldn't be possible to know how big it has to be).